package com.company;

import com.company.data.*;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
	// write your code here
        final Rose rose = new Rose(1000, 6, 13);
        final Tulip tulip = new Tulip(800, 9, 20);
        final Gypsophila gypsophila = new Gypsophila(2000, 10, 118);

        ArrayList<Flower> flowers1 = new ArrayList<>();
        flowers1.add(rose);
        flowers1.add(gypsophila);

        ArrayList<Flower> flowers2 = new ArrayList<>();
        flowers2.add(rose);
        flowers2.add(tulip);

        ArrayList<Flower> flowers3 = new ArrayList<>();
        flowers3.add(rose);
        flowers3.add(tulip);
        flowers3.add(gypsophila);


        final Flavor flavor1 = new Flavor(flowers1);
        final Flavor flavor2 = new Flavor(flowers2);
        final Flavor flavor3 = new Flavor(flowers3);

        final Flower flowerWithStemLengthLessThen15 = flavor1.getFlowerByStemLength(15);
        System.out.println(flowerWithStemLengthLessThen15);

        flavor3.sortFlowerByFreshIndex();

        final int getPriceOfFlavor2 = flavor2.getPrice();
        System.out.println(getPriceOfFlavor2);
    }
}
