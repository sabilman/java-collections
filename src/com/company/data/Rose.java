package com.company.data;

public class Rose implements  Flower{
    final private int price;
    final private int freshIndex;
    final private int stemLength;


    public Rose(int price, int freshIndex, int stemLength) {
        this.price = price;
        this.freshIndex = freshIndex;
        this.stemLength = stemLength;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public int getFreshIndex() {
        return freshIndex;
    }

    @Override
    public int getStemLength() {
        return stemLength;
    }
}
