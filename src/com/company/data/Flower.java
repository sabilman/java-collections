package com.company.data;

public interface Flower {
    int getPrice();
    int getFreshIndex();
    int getStemLength();
}
