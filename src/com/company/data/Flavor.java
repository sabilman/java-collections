package com.company.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Flavor {
    private ArrayList<Flower> flowers;

    public Flavor(ArrayList<Flower> flowers) {
        this.flowers = flowers;
    }

    public int getPrice(){
        int price = 0;
        for(int i = 0; i<flowers.size(); i ++){
            price+=flowers.get(i).getPrice();
        }
        return price;
    }

    public void sortFlowerByFreshIndex(){
        System.out.println("Unsorted Array -> " + flowers);
        Collections.sort(flowers, new Comparator<Flower>() {
            @Override
            public int compare(Flower o1, Flower o2) {
                return o2.getFreshIndex()-o1.getFreshIndex();
            }
        });
        System.out.println("Sorted Array -> " + flowers);

    }

    public Flower getFlowerByStemLength(int stemLength){
        for(Flower f : flowers){
            if(f.getStemLength()<=stemLength){
                return f;
            }
        }

        //Better to throw exception like no flowers with provided stem length
        return null;
    }

}
